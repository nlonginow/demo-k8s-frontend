package planning;


import java.util.logging.Logger;

import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration

public class LoadDatabase {
	private static final Logger logger = Logger.getLogger(LoadDatabase.class.getName());

	@Bean
	CommandLineRunner initDatabase(OutingRepository repository) {
		return args -> {

		    // The data to write to the log (will show up in Stackdriver)
		    logger.info("Preloading " + repository.save(new Outing("January", "Sledding")) + "\n");
		    logger.info("Preloading " +  repository.save(new Outing("February", "Tubing")) + "\n" );
		    
		};
	}
}
